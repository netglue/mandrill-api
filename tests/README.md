# Tests

At the moment, these tests require a mandrill account and network access to run.

There are few tests, and those here don't prove much.

The plan is to test properly using mocks and following the guide on Guzzle Docs.
Fancy helping? http://docs.guzzlephp.org/en/latest/testing/unit-testing.html


