<?php

namespace NetglueMandrillTest;

use PHPUnit_Framework_TestCase;

use NetglueMandrill\Client\MandrillClient;

class BasicTest extends PHPUnit_Framework_TestCase {

	protected $key;
	
	public function __construct() {
		$this->key = $_SERVER['API_KEY'];
	}
	
	
	/**
	 * @expectedException NetglueMandrill\Exception\InvalidApiKeyException
	 */
	public function testInvalidApiKeyException() {
		$client = new MandrillClient('InvalidApiKey');
		$result = $client->ping();
	}
	
	public function testClientConnectivity() {
		$client = new MandrillClient($this->key);
		$result = $client->ping();
		$this->assertInternalType('array', $result, 'Expected an array to be returned from the remote service');
	}
	
	public function testSendMessageWithMissingRequiredVar() {
		$client = new MandrillClient($this->key);
		$struct = array(
			'message' => array(
				'text' => 'Plain Text Alternative',
				'subject' => 'Mandrill API Test',
				'from_email' => 'george@net-glue.co.uk',
				'from_name' => 'George',
				'to' => array(
					array(
						'email' => 'gsteel@gmail.com',
						'name' => 'George',
						'type' => 'to',
					),
				),
			),
		);
		$result = $client->sendMessage($struct);
	}
	
	public function testMessageSend() {
		$client = new MandrillClient($this->key);
		$struct = array(
			'message' => array(
				'html' => '<p>Test Html</p><p>*|MERGE_1|*</p><p>*|MERGE_2|*</p><p>*|PERSONAL_MERGE_1|*</p><p>*|PERSONAL_MERGE_2|*</p><p>Link: <a href="http://netglue.co">Net Glue Website</a></p>',
				'text' => 'Plain Text Alternative',
				'subject' => 'Mandrill API Test',
				'from_email' => 'george@net-glue.co.uk',
				'from_name' => 'George',
				'to' => array(
					array(
						'email' => 'gsteel@gmail.com',
						'name' => 'George',
						'type' => 'to',
					),
					array(
						'email' => 'g@georgesteel.net',
						'name' => 'George',
						'type' => 'to',
					),
				),
				'headers' => array(
					'X-Extra-Header' => 'Foo',
					'X-Test-Header' => 'Bar',
				),
				'global_merge_vars' => array(
					array(
						'name' => 'MERGE_1',
						'content' => 'Merged Global Variable 1',
					),
					array(
						'name' => 'MERGE_2',
						'content' => 'Merged Global Variable 2',
					),
				),
				'merge_vars' => array(
					array(
						'rcpt' => 'gsteel@gmail.com',
						'vars' => array(
							array(
								'name' => 'PERSONAL_MERGE_1',
								'content' => 'gsteel@gmail.com Merged Personal Variable 1',
							),
							array(
								'name' => 'PERSONAL_MERGE_2',
								'content' => 'gsteel@gmail.com Merged Personal Variable 2',
							),
						),
					),
					array(
						'rcpt' => 'g@georgesteel.net',
						'vars' => array(
							array(
								'name' => 'PERSONAL_MERGE_1',
								'content' => 'g@georgesteel.net Merged Personal Variable 1',
							),
							array(
								'name' => 'PERSONAL_MERGE_2',
								'content' => 'g@georgesteel.net Merged Personal Variable 2',
							),
						),
					),
				),
				'tags' => array(
					'test',
				),
				'google_analytics_domains' => array('netglue.co'),
				'google_analytics_campaign' => 'mandrill_testing',
			),
		);
		$result = $client->sendMessage($struct);
		$this->assertInternalType('array', $result, 'Expected an array to be returned from the remote service');
	}
	
	public function testSendTemplate() {
		$client = new MandrillClient($this->key);
		$struct = array(
			'template_name' => 'netglue-contact',
			'template_content' => array(),
			'message' => array(
				'subject' => 'Mandrill API Template Test',
				'from_email' => 'george@net-glue.co.uk',
				'from_name' => 'George',
				'to' => array(
					array(
						'email' => 'gsteel@gmail.com',
						'name' => 'George',
						'type' => 'to',
					),
					array(
						'email' => 'g@georgesteel.net',
						'name' => 'George',
						'type' => 'to',
					),
				),
				'headers' => array(
					'X-Extra-Header' => 'Foo',
					'X-Test-Header' => 'Bar',
				),
				'auto_text' => true,
				'merge_vars' => array(
					array(
						'rcpt' => 'gsteel@gmail.com',
						'vars' => array(
							array(
								'name' => 'FNAME',
								'content' => 'FIRST NAME FOR gsteel@gmail.com',
							),
						),
					),
					array(
						'rcpt' => 'g@georgesteel.net',
						'vars' => array(
							array(
								'name' => 'FNAME',
								'content' => 'FIRST NAME FOR g@georgesteel.net',
							),
						),
					),
				),
				'tags' => array(
					'test',
				),
				'google_analytics_domains' => array('netglue.co'),
				'google_analytics_campaign' => 'mandrill_testing',
			),
		);
		$result = $client->sendTemplate($struct);
	}
	
	/**
	 * @expectedException NetglueMandrill\Exception\UnknownTemplateException
	 */
	public function testUnknownTemplateException() {
		$client = new MandrillClient($this->key);
		$struct = array(
			'template_name' => uniqid(),
			'template_content' => array(),
			'message' => array(),
		);
		$result = $client->sendTemplate($struct);
	}
}

