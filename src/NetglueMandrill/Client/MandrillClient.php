<?php

namespace NetglueMandrill\Client;

use NetglueMandrill\Version;
use NetglueMandrill\Listener\ErrorHandlerListener;

use Guzzle\Service\Client;
use Guzzle\Service\Description\ServiceDescription;


class MandrillClient extends Client {
	
	/**
	 * Current Mandrill API Version
	 */
	const API_VERSION = '1.0';
	
	const BASE_URL = 'https://mandrillapp.com/api';
	
	/**
	 * Constructor
	 * @param string $apiKey
	 * @param string $version
	 */
	public function __construct($apiKey, $version = self::API_VERSION) {
		
		/**
		 * Every request requires the API Key parameter
		 */
		
		$baseUrl = '';
		$config = array(
			'command.params' => array(
				'api_key' => $apiKey,
			),
		);
		
		parent::__construct($baseUrl, $config);
		
		$serviceConfig = sprintf('%s/../ServiceDescription/Mandrill-%s.php',
			__DIR__,
			$version);
		
		$serviceDescription = ServiceDescription::factory($serviceConfig);
		
		$this->setDescription($serviceDescription);
		
		// Set User Agent
		$this->setUserAgent('NetglueMandrill/' . Version::VERSION, true);
		
		// Base Url is versioned
    $this->setBaseUrl(sprintf('%s/%s', self::BASE_URL, $version));
		
		/**
		 * @todo Subscriber Error Handler
		 */
		$this->getEventDispatcher()->addSubscriber(new ErrorHandlerListener());
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function __call($method, $args = array()) {
		return parent::__call(ucfirst($method), $args);
	}
	
}
