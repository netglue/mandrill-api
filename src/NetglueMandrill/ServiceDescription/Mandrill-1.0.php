<?php

/**
 * Service Description for the Mandrill API
 */

$paramApiKey = array(
	'description' => 'Mandrill API key',
	'location'    => 'json',
	'type'        => 'string',
	'sentAs'      => 'key',
	'required'    => true
);
$paramMessage = array(
	'description' => 'The information on the message to send',
	'location'    => 'json',
	'type'        => 'array',
	'required'    => true,
	'items'       => array(
		'html' => array(
			'description' => 'Message HTML',
			'type'        => 'string',
			'required'    => true,
		),
		'text' => array(
			'description' => 'Message Plain Text Alternative',
			'type'        => 'string',
			'required'    => false,
		),
		'subject' => array(
			'description' => 'Message Subject',
			'type'        => 'string',
			'required'    => true,
		),
		'from_email' => array(
			'description' => 'Sender Email Address',
			'type'        => 'string',
			'required'    => true,
		),
		'from_name' => array(
			'description' => 'Sender Name',
			'type'        => 'string',
			'required'    => false,
		),
		'to' => array(
			'description' => 'Recipient Structure',
			'type'        => 'array',
			'required'    => true,
			'items' => array(
				'email' => array(
					'description' => 'Recipient Email Address',
					'type'        => 'string',
					'required'    => true,
				),
				'name' => array(
					'description' => 'Recipient Name',
					'type'        => 'string',
					'required'    => false,
				),
				'type' => array(
					'description' => 'Header type for the recipient, one of to, cc, bcc',
					'type'        => 'string',
					'required'    => false,
				),
			),
		),
		'headers' => array(
			'description' => 'An array of header/value pairs',
			'type'        => 'array',
			'required'    => false,
		),
		'important' => array(
			'description' => 'whether or not this message is important, and should be delivered ahead of non-important messages',
			'type'        => 'boolean',
			'required'    => false,
		),
		'track_opens' => array(
			'description' => 'whether or not to turn on open tracking for the message',
			'type'        => 'boolean',
			'required'    => false,
		),
		'track_clicks' => array(
			'description' => 'whether or not to turn on click tracking for the message',
			'type'        => 'boolean',
			'required'    => false,
		),
		'auto_text' => array(
			'description' => 'whether or not to automatically generate a text part for messages that are not given text',
			'type'        => 'boolean',
			'required'    => false,
		),
		'auto_html' => array(
			'description' => 'whether or not to automatically generate an HTML part for messages that are not given HTML',
			'type'        => 'boolean',
			'required'    => false,
		),
		'inline_css' => array(
			'description' => 'whether or not to automatically inline all CSS styles provided in the message HTML - only for HTML documents less than 256KB in size',
			'type'        => 'boolean',
			'required'    => false,
		),
		'url_strip_qs' => array(
			'description' => 'whether or not to strip the query string from URLs when aggregating tracked URL data',
			'type'        => 'boolean',
			'required'    => false,
		),
		'preserve_recipients' => array(
			'description' => 'whether or not to expose all recipients in to "To" header for each email',
			'type'        => 'boolean',
			'required'    => false,
		),
		'view_content_link' => array(
			'description' => 'set to false to remove content logging for sensitive emails',
			'type'        => 'boolean',
			'required'    => false,
		),
		'bcc_address' => array(
			'description' => 'an optional address to receive an exact copy of each recipient\'s email',
			'type'        => 'string',
			'required'    => false,
		),
		'tracking_domain' => array(
			'description' => 'a custom domain to use for tracking opens and clicks instead of mandrillapp.com',
			'type'        => 'string',
			'required'    => false,
		),
		'signing_domain' => array(
			'description' => 'a custom domain to use for SPF/DKIM signing instead of mandrill (for "via" or "on behalf of" in email clients)',
			'type'        => 'string',
			'required'    => false,
		),
		'return_path_domain' => array(
			'description' => 'a custom domain to use for the messages\'s return-path',
			'type'        => 'string',
			'required'    => false,
		),
		'merge' => array(
			'description' => 'whether to evaluate merge tags in the message. Will automatically be set to true if either merge_vars or global_merge_vars are provided.',
			'type'        => 'boolean',
			'required'    => false,
		),
		'global_merge_vars' => array(
			'description' => 'global merge variables to use for all recipients. You can override these per recipient.',
			'type'        => 'array',
			'required'    => false,
			'items' => array(
				array(
					'type' => 'array',
					'items' => array(
						'name' => array(
							'type' => 'string',
							'required' => true,
						),
						'content' => array(
							'type' => 'string',
							'required' => true,
						),
					),
				),
			),
		),
		'merge_vars' => array(
			'description' => 'per-recipient merge variables, which override global merge variables with the same name.',
			'type'        => 'array',
			'required'    => false,
			'items' => array(
				array(
					'type' => 'array',
					'items' => array(
						'rcpt' => array(
							'description' => 'the email address of the recipient that the merge variables should apply to',
							'type' => 'string',
							'required' => true,
						),
						'vars' => array(
							'description' => 'the recipient\'s merge variables',
							'type' => 'array',
							'required' => true,
							'items' => array(
								array(
									'type' => 'array',
									'items' => array(
										'name' => array(
											'type' => 'string',
											'required' => true,
										),
										'content' => array(
											'type' => 'string',
											'required' => true,
										),
									),
								),
							),
						),
					),
				),
			),
		),
		'tags' => array(
			'description' => 'an array of string to tag the message with. Stats are accumulated using tags, though we only store the first 100 we see, so this should not be unique or change frequently. Tags should be 50 characters or less. Any tags starting with an underscore are reserved for internal use and will cause errors.',
			'type'        => 'array',
			'required'    => false,
		),
		'subaccount' => array(
			'description' => 'the unique id of a subaccount for this message - must already exist or will fail with an error',
			'type'        => 'string',
			'required'    => false,
		),
		'google_analytics_domains' => array(
			'description' => 'an array of strings indicating for which any matching URLs will automatically have Google Analytics parameters appended to their query string automatically.',
			'type'        => 'array',
			'required'    => false,
		),
		'google_analytics_campaign' => array(
			'description' => 'optional string indicating the value to set for the utm_campaign tracking parameter. If this isn\'t provided the email\'s from address will be used instead.',
			'type'        => 'array|string',
			'required'    => false,
		),
		'metadata' => array(
			'description' => 'An associative array of user metadata. Mandrill will store this metadata and make it available for retrieval. In addition, you can select up to 10 metadata fields to index and make searchable using the Mandrill search api.',
			'type'        => 'array',
			'required'    => false,
		),
		'recipient_metadata' => array(
			'description' => 'Per-recipient metadata that will override the global values specified in the metadata parameter.',
			'type'        => 'array',
			'required'    => false,
			'items' => array(
				array(
					'type' => 'array',
					'items' => array(
						'rcpt' => array(
							'type' => 'string',
							'description' => 'the email address of the recipient that the metadata is associated with',
							'required' => true,
						),
						'values' => array(
							'type' => 'array',
							'description' => 'an associated array containing the recipient\'s unique metadata. If a key exists in both the per-recipient metadata and the global metadata, the per-recipient metadata will be used.',
							'required' => true,
						),
					),
				),
			),
		),
		'attachments' => array(
			'description' => 'an array of supported attachments to add to the message',
			'type'        => 'array',
			'required'    => false,
			'items' => array(
				array(
					'type' => 'array',
					'items' => array(
						'type' => array(
							'type' => 'string',
							'description' => 'the MIME type of the attachment',
							'required' => true,
						),
						'name' => array(
							'type' => 'string',
							'description' => 'the file name of the attachment',
							'required' => true,
						),
						'content' => array(
							'type' => 'string',
							'description' => 'the content of the attachment as a base64-encoded string',
							'required' => true,
						),
					),
				),
			),
		),
		'images' => array(
			'description' => 'an array of embedded images to add to the message',
			'type'        => 'array',
			'required'    => false,
			'items' => array(
				array(
					'type' => 'array',
					'items' => array(
						'type' => array(
							'type' => 'string',
							'description' => 'the MIME type of the image - must start with "image/"',
							'required' => true,
						),
						'name' => array(
							'type' => 'string',
							'description' => 'the Content ID of the image - use <img src="cid:THIS_VALUE"> to reference the image in your HTML content',
							'required' => true,
						),
						'content' => array(
							'type' => 'string',
							'description' => 'the content of the image as a base64-encoded string',
							'required' => true,
						),
					),
				),
			),
		),
	),
);

return array(
	
	'name'        => 'Mandrill',
	'apiVersion'  => '1.0',
	'description' => 'Mandrill is a cloud based service for sending transactional email messages, made by the MailChimp people',
	'operations'  => array(
		
		/**
		 * User Related
		 */
		
		
		'Ping' => array(
			'httpMethod' => 'POST',
			'uri' => 'users/ping2.json',
			'summary' => 'Validate an API key and respond to a ping',
			'documentationUrl' => 'https://mandrillapp.com/api/docs/users.JSON.html#method=ping',
			'parameters' => array(
				'api_key'  => $paramApiKey,
			),
		),
		
		'GetUserInfo' => array(
			'extends' => 'Ping',
			'uri' => 'users/info.json',
			'summary' => 'Return the information about the API-connected user',
			'documentationUrl' => 'https://mandrillapp.com/api/docs/users.JSON.html',
		),
		
		/**
		 * Message Related
		 */
		
		
		'SendMessage' => array(
			'httpMethod' => 'POST',
			'uri' => 'messages/send.json',
			'summary' => 'Send a new transactional message through Mandrill',
			'documentationUrl' => 'https://mandrillapp.com/api/docs/messages.JSON.html#method=send',
			'parameters' => array(
				'api_key'  => $paramApiKey,
				'message' => $paramMessage,
				'async' => array(
					'description' => 'Enable a background sending mode that is optimized for bulk sending. In async mode, messages/send will immediately return a status of "queued" for every recipient. To handle rejections when sending in async mode, set up a webhook for the \'reject\' event. Defaults to false for messages with no more than 10 recipients; messages with more than 10 recipients are always sent asynchronously, regardless of the value of async.',
					'location'    => 'json',
					'type'        => 'boolean',
					'sentAs'      => 'async',
					'required'    => false,
				),
				'ip_pool' => array(
					'description' => 'the name of the dedicated ip pool that should be used to send the message. If you do not have any dedicated IPs, this parameter has no effect. If you specify a pool that does not exist, your default pool will be used instead.',
					'location'    => 'json',
					'type'        => 'string',
					'sentAs'      => 'ip_pool',
					'required'    => false,
				),
				'send_at' => array(
					'description' => 'when this message should be sent as a UTC timestamp in YYYY-MM-DD HH:MM:SS format. If you specify a time in the past, the message will be sent immediately. An additional fee applies for scheduled email, and this feature is only available to accounts with a positive balance. Validation: datetime',
					'location'    => 'json',
					'type'        => 'string',
					'sentAs'      => 'send_at',
					'required'    => false,
				),
			),
			
			
		),
		
		'SendTemplate' => array(
			'httpMethod' => 'POST',
			'uri' => 'messages/send-template.json',
			'summary' => 'Send a new transactional message through Mandrill using a template',
			'documentationUrl' => 'https://mandrillapp.com/api/docs/messages.JSON.html#method=send-template',
			'parameters' => array(
				'api_key'  => $paramApiKey,
				'template_name' => array(
					'description' => 'the immutable name or slug of a template that exists in the user\'s account. For backwards-compatibility, the template name may also be used but the immutable slug is preferred.',
					'type'        => 'string',
					'required'    => true,
					'location'    => 'json',
				),
				'template_content' => array(
					'description' => 'an array of template content to send. Each item in the array should be a struct with two keys - name: the name of the content block to set the content for, and content: the actual content to put into the block',
					'type'        => 'array',
					'required'    => true,
					'location'    => 'json',
					'items' => array(
						array(
							'description' => 'the injection of a single piece of content into a single editable region',
							'type' => 'array',
							'items' => array(
								'name' => array(
									'description' => 'the name of the mc:edit editable region to inject into',
									'type' => 'string',
									'required' => true,
								),
								'content' => array(
									'description' => 'the content to inject',
									'type' => 'string',
									'required' => true,
								),
							),
						),
					),
				),
				'message' => $paramMessage,
				'async' => array(
					'description' => 'Enable a background sending mode that is optimized for bulk sending. In async mode, messages/send will immediately return a status of "queued" for every recipient. To handle rejections when sending in async mode, set up a webhook for the \'reject\' event. Defaults to false for messages with no more than 10 recipients; messages with more than 10 recipients are always sent asynchronously, regardless of the value of async.',
					'location'    => 'json',
					'type'        => 'boolean',
					'sentAs'      => 'async',
					'required'    => false,
				),
				'ip_pool' => array(
					'description' => 'the name of the dedicated ip pool that should be used to send the message. If you do not have any dedicated IPs, this parameter has no effect. If you specify a pool that does not exist, your default pool will be used instead.',
					'location'    => 'json',
					'type'        => 'string',
					'sentAs'      => 'ip_pool',
					'required'    => false,
				),
				'send_at' => array(
					'description' => 'when this message should be sent as a UTC timestamp in YYYY-MM-DD HH:MM:SS format. If you specify a time in the past, the message will be sent immediately. An additional fee applies for scheduled email, and this feature is only available to accounts with a positive balance. Validation: datetime',
					'location'    => 'json',
					'type'        => 'string',
					'sentAs'      => 'send_at',
					'required'    => false,
				),
			),
		),
		
		
	),
	
	
);