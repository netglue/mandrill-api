<?php

namespace NetglueMandrill\Exception;

use Guzzle\Plugin\ErrorResponse\ErrorResponseExceptionInterface;

interface ExceptionInterface extends ErrorResponseExceptionInterface {
	
	public function getResponseBody();
	public function setResponseBody($body);
}
