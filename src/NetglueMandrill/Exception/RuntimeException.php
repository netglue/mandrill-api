<?php

namespace NetglueMandrill\Exception;

use Guzzle\Service\Command\CommandInterface;
use Guzzle\Http\Message\Response;

class RuntimeException extends \RuntimeException implements ExceptionInterface {
	
	protected $responseBody;
	
	/**
	 * Create an exception for a command based on a command and an error response definition
	 *
	 * @param CommandInterface $command  Command that was sent
	 * @param Response         $response The error response
	 *
	 * @return self
	 */
	public static function fromCommand(CommandInterface $command, Response $response) {
		$result = json_decode($response->getBody(), true);
		$errorName = isset($result['name']) ? $result['name'] : 'Unknown_Exception';
		$ex = new RuntimeException($result['message'], $result['code'], 'Mandrill returned the error '.$errorName);
		$ex->setResponseBody($result);
		throw $ex;
	}
	
	public function setResponseBody($body) {
		$this->responseBody = $body;
		return $this;
	}
	
	public function getResponseBody() {
		return $this->responseBody;
	}
}
