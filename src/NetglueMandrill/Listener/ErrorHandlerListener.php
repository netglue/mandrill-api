<?php

namespace NetglueMandrill\Listener;

use Guzzle\Common\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ErrorHandlerListener implements EventSubscriberInterface {
	
	private $errorMap = array(
		'Invalid_Key'        => 'NetglueMandrill\Exception\InvalidApiKeyException',
		'Unknown_Exception'  => 'NetglueMandrill\Exception\UnknownException',
		'ValidationError'    => 'NetglueMandrill\Exception\ValidationException',
		'GeneralError'       => 'NetglueMandrill\Exception\GeneralErrorException',
		'PaymentRequired'    => 'NetglueMandrill\Exception\PaymentRequiredException',
		'Unknown_Subaccount' => 'NetglueMandrill\Exception\UnknownSubaccountException',
		'Unknown_Template'   => 'NetglueMandrill\Exception\UnknownTemplateException',
		'Invalid_Template'   => 'NetglueMandrill\Exception\InvalidTemplateException',
	);
	
	/**
	 * Return events to subscribe to
	 * @return array
	 */
	public static function getSubscribedEvents() {
		 return array('request.exception' => 'handleError');
	}
	
	/**
	 * @internal
	 * @param  Event $event
	 * @return void
	 * @throws \NetglueMandrill\Exception\ExceptionInterface
	 */
	public function handleError(Event $event) {
		$response = $event['response'];
		
		if($response->getStatusCode() === 200) {
			return;
		}
		$result = json_decode($response->getBody(), true);
		$errorName = isset($result['name']) ? $result['name'] : 'Unknown_Exception';
		
		$previous = isset($event['exception']) ? $event['exception'] : NULL;
		$ex = new $this->errorMap[$errorName]($result['message'], $result['code'], $previous);
		$ex->setResponseBody($result);
		
		throw $ex;
	}
	
}
